package reader;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Reader_ui extends JFrame implements ActionListener, ChangeListener{
	Reader_core corePdf;
	JTextArea pageArea;
	JButton btnSelectFile,btnNext,btnPrev,btnGoto,btnSearch;
	JTextField tfGoto;
	JTextField tfSearch;
	JSlider jpbCurPos;
	JPanel panNav;	
	
	JScrollPane scroll;
	JPanel panProg;
	JLabel lblInfo;
public Reader_ui(String title){
	super(title);
	setSize(700,500);
	java.awt.Dimension maxSize = new java.awt.Dimension(700,700);
	setMinimumSize(maxSize);

	setLayout(new BorderLayout());
	pageArea = new JTextArea();
	pageArea.setRows(20);
	pageArea.setColumns(60);
	scroll = new JScrollPane (pageArea);
	jpbCurPos = new JSlider();
	panNav = new JPanel();
	
	btnSelectFile = new JButton("Open File");
	btnPrev = new JButton("<<<<");
	tfSearch = new JTextField(17);
	btnSearch = new JButton("Search");
	tfGoto = new JTextField(5);
	btnGoto = new JButton("GO");
	btnNext = new JButton(">>>>");
	
	lblInfo = new JLabel();
	panProg = new JPanel();
	panProg.setLayout(new GridLayout(1,2));
	panProg.add(lblInfo);
	panProg.add(jpbCurPos);
		
	panNav.setLayout(new FlowLayout());
	panNav.add(btnSelectFile);
	panNav.add(tfSearch);
	panNav.add(btnSearch);
	panNav.add(tfGoto);
	panNav.add(btnGoto);
	panNav.add(btnPrev);
	panNav.add(btnNext);

add(panNav,BorderLayout.PAGE_START);
add(scroll,BorderLayout.CENTER);
add(panProg,BorderLayout.PAGE_END);

	jpbCurPos.addChangeListener(this);
	btnPrev.addActionListener(this);
	btnNext.addActionListener(this);
	btnSelectFile.addActionListener(this);
	btnGoto.addActionListener(this);
	btnSearch.addActionListener(this);
	
}
	public void stateChanged(ChangeEvent e) {
	    int page= jpbCurPos.getValue();
	    try {
			pageArea.setText(corePdf.getPage(page));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    updateProgress();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	
		Object pressedButton = e.getSource();
		if(pressedButton==btnNext){
			try {
				printNextPage();
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			updateProgress();
			}
		else if(pressedButton==btnSelectFile){
			try {
					openFile();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			updateProgress();
			}
		else if(pressedButton == btnPrev){
			try{
				printPrevPage();
			}catch (Exception e3){
				e3.printStackTrace();
			}
			updateProgress();
		}
		else if(pressedButton ==btnGoto){
			
			int page = Integer.parseInt(tfGoto.getText());
			try {
				pageArea.setText(corePdf.getPage(page));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			updateProgress();
		}
		else if(pressedButton == btnSearch){
			String searchItem = tfSearch.getText().toString();
			int page=-1;
			try {
				page = findWhereIam(searchItem,corePdf.getCurrentPageNo());
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			if(page == -1){
			JOptionPane.showMessageDialog(this, "String "+searchItem+" not found!\nTry changing the search term.");
			return;
			}
			corePdf.setCurrentPageNo(page);
			try {
				pageArea.setText(corePdf.getCurrent());
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			updateProgress();
		}
		
		
	}
	public int findWhereIam(String searchItem,int startPage) throws Exception{
		int maxPage = corePdf.getTotalPages();
		for(int i=startPage;i<=maxPage;i++){
			corePdf.setCurrentPageNo(i);
			String pageCont = corePdf.getCurrent();
			if(pageCont.contains(searchItem))
				return i;
		}
		return -1;
	}
	public void printNextPage() throws Exception{
		pageArea.setText(corePdf.getNext());
		updateProgress();
	}
	public void printPrevPage() throws Exception {
		pageArea.setText(corePdf.getPrev());
		updateProgress();
	}
	public void openFile() throws Exception{

		JFileChooser pdfChooser = new JFileChooser();
		pdfChooser.setDialogTitle("Select a pdf file to open ");
		pdfChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		pdfChooser.showOpenDialog(this);
		String file = pdfChooser.getSelectedFile().toString();
		corePdf = new Reader_core(file);
		pageArea.setText(corePdf.getCurrent());
	
		jpbCurPos.setMaximum(corePdf.getTotalPages());
		updateProgress();
		
	}
	public void updateProgress()
	{
		lblInfo.setText("Page "+corePdf.getCurrentPageNo()+"/"+corePdf.getTotalPages()+" ... ");
		jpbCurPos.setValue(corePdf.getCurrentPageNo());				
	}
	public static void main(String[] args) {
		Reader_ui app1 = new Reader_ui("PDF Redaer || Text Mode only. ");
		app1.setVisible(true);
		app1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}

}
