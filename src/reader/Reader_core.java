package reader;

import java.io.IOException;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;

public class Reader_core {
	private int currentPageNo;
	private int NoOfPageses;
	private String currentPage;
	private String nextPage;
	private String prevPage;
	public PdfReader pdf;
	
	public int getTotalPages(){
		return NoOfPageses;
	}
	public String getNextPage()	{
		return nextPage;
	}
	public String prevPage(){
		return prevPage;
	}
	public int getCurrentPageNo(){
		return currentPageNo;
	}
	public void setCurrentPageNo(int s){
		this.currentPageNo=s;
	}
public Reader_core(String filename) throws IOException{
this.pdf = new PdfReader(filename);
this.NoOfPageses = pdf.getNumberOfPages();
this.currentPageNo=1;
}
public String getCurrent() throws Exception {
	return PdfTextExtractor.getTextFromPage(pdf,currentPageNo);
}
public String getPage(int page) throws Exception{
	if((page > NoOfPageses) || (page < 0))
		throw new Exception();
	currentPageNo = page;
	
	return PdfTextExtractor.getTextFromPage(pdf, page);
}
public String getNext() throws Exception{
	if(currentPageNo == NoOfPageses)
		throw new Exception();
	currentPageNo +=1;
	return PdfTextExtractor.getTextFromPage(pdf, currentPageNo);
}
public String getPrev() throws Exception {
	if(currentPageNo == 1)
		throw new Exception();
	currentPageNo -= 1;
	return PdfTextExtractor.getTextFromPage(pdf, currentPageNo);
}
}
